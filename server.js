const express = require('express');
const app = express();
const request = require('request');
const cheerio = require('cheerio');
const TurndownService = require('turndown')
const turndownService = new TurndownService()

app.get('/', function(req, res) {
	var headers = {
		'user-agent':
			'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
		Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
	};

	var options = {
		uri: 'https://github.com/Fast0n/WiFiView/releases',
		method: 'GET',
		json: true,
		headers: headers,
	};

	request(options, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			const $ = cheerio.load(body);

			var json = '';
			var array = [];
			var array1 = [];

			var a = 0;
			var b = 0;

			var title = [];
			var desc = [];

			var results = $('div.application-main ');
			results.each(function(i, result) {
				// take a version
				$(result)
					.find('div.release-body')
					.find('h1')
					.find('a')
					.each(function(index, element) {
						array = array.concat([$(element).html()]);
					});

				if (array != '') {
					title[a] = array;
					a++;
				}

				/**
				 * result = title[c]
				 *
				 **/

				$(result)
					.find('div.markdown-body')
					.each(function(index, element) {
						array1 = array1.concat([$(element).html()]);
					});

				if (array1 != '') {
					desc[b] = array1;
					b++;
				}

				for (var j = 0; j < title[0].length; j++) {
					console.log(desc[0][j]);
          json += '{ "version": "' + title[0][j].replace("","") + '", "description": "' + desc[0][j] + '"},';
				}
			});
		} else {
			res.json(response.statusCode);
		}

		json = '{ "release": [' + turndownService.turndown(json) + ']}';
    json=json.replace('"},]}', '"}]}').replace(/====/g, '');
    json = json.replace(/= */g, ' ');
		res.send(json);
	});
});

const server = app.listen(process.env.PORT, function() {});
